Converting Bartik to Bartik Responsive
Tentative roadmap

1. Change theme name/.info file for dev work and no conflict with existing Bartik - DONE
2. Convert all px based containers and columns to %. Change overall layout to fluid with l/r margins. Progressively enhance Bartik for larger screen sizes.
-- http://drupal.org/node/1192044
-- http://drupal.org/node/1192064
4. Change all font references to em's with appropriate  calculations for fonts within containers
5. Add media query css files for greater control.
-- Strip styles down to a mobile friendly layout. http://drupal.org/node/1192054
-- Provide graceful degradation for browsers that do not support mq. http://drupal.org/node/1192068

TODO
Begin thought around Fluid Images and how they will be baked in, keeping an eye on http://drupal.org/sandbox/johnalbin/1110358. Images are a major issue
testing git to fire up new project
